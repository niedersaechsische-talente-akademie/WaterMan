package de.workshop.water;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;


public class WaterMan extends ApplicationAdapter {
    SpriteBatch batch;
    OrthographicCamera camera;
    Music waves;
    GameState gameState = GameState.Menu;
    float time = 0;
    int score = 0;
    //Texture background;
    Texture[][] raster;
    Texture water;
    Texture island;
    Texture finish;
    Texture start;
    Texture log;
    Texture logshortinv;
    Texture loglonginv;
    Texture stay;
    Texture moveleft;
    Texture moveright;
    Texture frame;
    Vector2 playerPosition;
    Animation player;
    Vector2 playerTargetPosition;
    Texture menu;
    int loginv;
    BitmapFont font;
    Animation sharkanm;
    Texture shark1;
    Texture shark2;
    Texture shark3;
    Texture shark4;
    Texture shark5;
    Texture shark6;
    Texture shark7;
    Texture shark8;
    Texture shark9;
    Texture shark10;
    Texture gameOver;
    Texture gameSurvived;
    Texture infoMenu;
    Texture settingsSoundOnHardmodeOff;
    Texture settingsSoundOffHardmodeOff;
    Texture settingsSoundOffHardmodeOn;
    Texture settingsSoundOnHardmodeOn;
    Texture history;
    Texture steering;
    Texture pauseButton;
    Texture pause;
    Texture islandlog;
    Texture number1;
    Texture number2;
    Texture number3;
    Texture number4;
    Texture number5;
    Texture number6;
    Texture number7;
    Texture number8;
    Texture number9;
    Texture number10;
    Texture number11;
    Texture number0;
    Texture hardmodeinfo;
    Sound clap;
    Sound drop;
    Sound fail;
    boolean isClapPlaying;
    boolean isFailPlaying;
    boolean soundOn;
    boolean hardmodeOff;
    boolean isGivingLog;
    Texture[] numbers;
    Texture[] cannons;
    Texture kugel;

    private static class Cannonball {
        Vector2 position;
        Vector2 startPosition;
        Vector2 direction;

        Cannonball(float px, float py, int dir) {
            position = new Vector2(px, py);
            startPosition = new Vector2(px, py);
            float dx = 0;
            float dy = 0;
            if (dir == 0) {
                dx = -2;
                dy = 0;
            } else if (dir == 1) {
                dx = 0;
                dy = -2;
            } else if (dir == 2) {
                dx = 2;
                dy = 0;
            } else if (dir == 3) {
                dx = 0;
                dy = 2;
            }
            direction = new Vector2(dx, dy);
        }

        void step() {
            position.add(direction);
            if (position.x > 860 ||
                    position.x < -200 ||
                    position.y > 680 ||
                    position.y < -200) {
                position.set(startPosition);
            }
        }

        boolean hits(Vector2 player) {
            Vector2 kugelCenter = new Vector2(position.x + 40, position.y + 32);
            Rectangle playerRect = new Rectangle(player.x - 30, player.y - 30, 60, 60);
            return playerRect.contains(kugelCenter);
        }

    }

    Array<Cannonball> cannonballs;

    @Override
    public void create() {
        batch = new SpriteBatch();
        //background = new Texture("background.png");
        batch = new SpriteBatch();
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 480);
        water = new Texture("Wasser.png");
        island = new Texture("Insel_auf_Wasser.png");
        finish = new Texture("Ziel_mit_Schrift.png");
        start = new Texture("Start_weicher_rand_mit_Schrift.png");
        log = new Texture("Wasser_Brett.png");
        logshortinv = new Texture("Brett_Inventar_kurz.png");
        loglonginv = new Texture("Brett_Inventar_Lang.png");
        stay = new Texture("Kannibale.png");
        moveleft = new Texture("Kannibale_Schritt1.png");
        moveright = new Texture("Kannibale_Schritt2.png");
        frame = new Texture("Rahmen.png");
        player = new Animation(0.5f,
                new TextureRegion(moveleft),
                new TextureRegion(moveright));
        player.setPlayMode(PlayMode.LOOP);
        playerPosition = new Vector2();
        playerTargetPosition = new Vector2();
        menu = new Texture("Menu.png");
        font = new BitmapFont(Gdx.files.internal("arial.fnt"));
        shark1 = new Texture("shark1.png");
        shark2 = new Texture("shark2.png");
        shark3 = new Texture("shark3.png");
        shark4 = new Texture("shark4.png");
        shark5 = new Texture("shark5.png");
        shark6 = new Texture("shark6.png");
        shark7 = new Texture("shark7.png");
        shark8 = new Texture("shark8.png");
        shark9 = new Texture("shark9.png");
        shark10 = new Texture("shark10.png");
        sharkanm = new Animation(0.25f,
                new TextureRegion(shark1),
                new TextureRegion(shark2),
                new TextureRegion(shark3),
                new TextureRegion(shark4),
                new TextureRegion(shark5),
                new TextureRegion(shark6),
                new TextureRegion(shark7),
                new TextureRegion(shark8),
                new TextureRegion(shark9),
                new TextureRegion(shark10));
        sharkanm.setPlayMode(PlayMode.LOOP);
        gameOver = new Texture("Game Over Bild.png");
        gameSurvived = new Texture("You survived.png");
        clap = Gdx.audio.newSound(Gdx.files.internal("Klatschen Final.mp3"));
        drop = Gdx.audio.newSound(Gdx.files.internal("PlatschenFinal.mp3"));
        fail = Gdx.audio.newSound(Gdx.files.internal("Sad - Trombone.mp3"));
        waves = Gdx.audio.newMusic(Gdx.files.internal("Wellen Final.mp3"));
        infoMenu = new Texture("Info_Screen.png");
        history = new Texture("Geschichte.png");
        steering = new Texture("Steuerung.png");
        pauseButton = new Texture("Pause Button.png");
        pause = new Texture("Pause_Screen.png");
        islandlog = new Texture("Insel_mit_Holz.png");
        settingsSoundOnHardmodeOff = new Texture("settings-soundon-hardmodeoff.png");
        settingsSoundOffHardmodeOff = new Texture("settings-soundoff-hardmodeoff.png");
        settingsSoundOnHardmodeOn = new Texture("settings-soundon-hardmodeon.png");
        settingsSoundOffHardmodeOn = new Texture("settings-soundoff-hardmodeon.png");
        number1 = new Texture("number1.png");
        number2 = new Texture("number2.png");
        number3 = new Texture("number3.png");
        number4 = new Texture("number4.png");
        number5 = new Texture("number5.png");
        number6 = new Texture("number6.png");
        number7 = new Texture("number7.png");
        number8 = new Texture("number8.png");
        number9 = new Texture("number9.png");
        number10 = new Texture("number10.png");
        number11 = new Texture("number11.png");
        number0 = new Texture("number0.png");
        numbers = new Texture[12];
        numbers[0] = number0;
        numbers[1] = number1;
        numbers[2] = number2;
        numbers[3] = number3;
        numbers[4] = number4;
        numbers[5] = number5;
        numbers[6] = number6;
        numbers[7] = number7;
        numbers[8] = number8;
        numbers[9] = number9;
        numbers[10] = number10;
        numbers[11] = number11;
        cannons = new Texture[4];
        cannons[0] = new Texture("Kanone_auf_Insel_Links.png");
        cannons[1] = new Texture("Kanone_auf_Insel_unten.png");
        cannons[2] = new Texture("Kanone_auf_Insel_rechts.png");
        cannons[3] = new Texture("Kanone_auf_Insel_oben.png");
        soundOn = true;
        hardmodeOff = true;
        kugel = new Texture("Kanonenkugel.png");
        hardmodeinfo = new Texture("HardmodeInfo.png");
        resetWorld();
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        updateWorld();
        drawWorld();
    }

    enum GameState {
        Menu, Running, GameOver, Win, Ready, InfoMenu, Info,
        Einstellungen, Geschichte, Steuerung, Pause, HardModeInfo
    }

    
    
    private int neighbors(int x0, int y0) {
        int n = 0;
        for (int y = -1; y <= 1; y++) {
            for (int x = -1; x <= 1; x++) {
                if (x != 0 || y != 0) {
                    int x1 = x0 + x;
                    int y1 = y0 + y;
                    if (x1 >= 0 && x1 <= 10 && y1 >= 0 && y1 <= 7) {
                        if (raster[y1][x1] != null) {
                            n++;
                        }
                    }
                }
            }
        }
        return n;
    }

    
    
    private void resetWorld() {
        Gdx.app.debug("PlaneGame", camera.position.toString());
        camera.position.x = 800 / 2;
        camera.position.y = 480 / 2;
        time = 0;
        score = 0;
        playerPosition.set(30, 30);
        playerTargetPosition.set(30, 30);
        loginv = 7;

        cannonballs = new Array<Cannonball>();

        raster = new Texture[8][11];

        for (int i = 0; i < 15; i++) {
            int x = MathUtils.random(10);
            int y = MathUtils.random(7);
            Texture t = raster[y][x];
            if (t != null || x == 0 && y == 0 || x == 10 && y == 7) {
                i--;
            } else if (neighbors(x, y) > 3) {
                i--;
            } else {
                boolean isCannonIsland = !hardmodeOff && MathUtils.randomBoolean(0.25f);
                if (isCannonIsland) {
                    int j = MathUtils.random(cannons.length - 1);
                    raster[y][x] = cannons[j];
                    Cannonball k = new Cannonball(x * 60, y * 60, j);
                    cannonballs.add(k);
                } else {
                    raster[y][x] = island;
                }
            }
        }
        for (int i = 0; i < 1; i++) {
            int x = MathUtils.random(10);
            int y = MathUtils.random(7);
            Texture t = raster[y][x];
            if (t != null || x == 0 && y == 0 || x == 10 && y == 7) {
                i--;
            } else if (neighbors(x, y) > 1) {
                i--;
            } else {
                raster[y][x] = islandlog;
            }
        }
        for (int i = 0; i < 5; i++) {
            int x = MathUtils.random(10);
            int y = MathUtils.random(7);
            Texture t = raster[y][x];
            if (t != null || x == 0 && y == 0 || x == 10 && y == 7) {
                i--;
            } else {
                raster[y][x] = shark1;
            }
        }

        raster[0][0] = start;
        raster[7][10] = finish;

        isClapPlaying = false;
        isFailPlaying = false;
        isGivingLog = false;


    }

    
    
    private void updateWorld() {
        float deltaTime = Gdx.graphics.getDeltaTime();
        time += deltaTime;

        if (Gdx.input.justTouched()) {
            float x = Gdx.input.getX();
            float y = Gdx.input.getY();
            Vector3 coords = new Vector3(x, y, 0);
            camera.unproject(coords);
            x = coords.x;
            y = coords.y;

            if (gameState == GameState.Menu && x > 470 && x < 670 && y > 340 && y < 420) {
                gameState = GameState.Ready;
            } else if (gameState == GameState.Menu && x > 470 && x < 660 && y > 210 && y < 313) {
                gameState = GameState.InfoMenu;
            } else if (gameState == GameState.Ready) {
                gameState = GameState.Running;
            } else if (gameState == GameState.GameOver) {
                gameState = GameState.Menu;
                resetWorld();
            } else if (gameState == GameState.Win) {
                gameState = GameState.Menu;
                resetWorld();
            } else if (gameState == GameState.InfoMenu && x > 90 && x < 430 && y > 300 && y < 360) {
                gameState = GameState.Einstellungen;
            } else if (gameState == GameState.InfoMenu && x > 80 && x < 380 && y > 220 && y < 260) {
                gameState = GameState.Geschichte;
            } else if (gameState == GameState.InfoMenu && x > 90 && x < 350 && y > 110 && y < 170) {
                gameState = GameState.Steuerung;
            } else if (gameState == GameState.InfoMenu && x > 19 && x < 129 && y > 12 && y < 90) {
                gameState = GameState.Menu;
                resetWorld();
            } else if (gameState == GameState.Geschichte && x > 630 && x < 730 && y > 410 && y < 470) {
                gameState = GameState.InfoMenu;
            } else if (gameState == GameState.Steuerung && x > 40 && x < 140 && y > 20 && y < 80) {
                gameState = GameState.InfoMenu;
            } else if (gameState == GameState.Steuerung && x > 40 && x < 140 && y > 20 && y < 80) {
                gameState = GameState.InfoMenu;
            } else if (gameState == GameState.Running && x > 670 && x < 830 && y > 10 && y < 70) {
                gameState = GameState.Pause;
            } else if (gameState == GameState.Pause && x > 130 && x < 525 && y > 260 && y < 315) {
                gameState = GameState.Ready;
            } else if (gameState == GameState.Pause && x > 170 && x < 485 && y > 145 && y < 190) {
                gameState = GameState.Ready;
                resetWorld();
            } else if (gameState == GameState.Pause && x > 255 && x < 410 && y > 35 && y < 85) {
                gameState = GameState.Menu;
                resetWorld();
            } else if (gameState == GameState.Einstellungen && x > 30 && x < 150 && y > 25 && y < 100) {
                gameState = GameState.InfoMenu;
            } else if (gameState == GameState.Einstellungen && x > 25 && x < 245 && y > 315 && y < 370) {
                soundOn = !soundOn;
            } else if (gameState == GameState.Einstellungen && x > 25 && x < 300 && y > 240 && y < 290) {
                hardmodeOff = !hardmodeOff;
            } else if (gameState == GameState.Einstellungen && x > 310 && x < 360 && y > 240 && y < 290) {
                gameState = GameState.HardModeInfo;
            } else if (gameState == GameState.HardModeInfo && x > 30 && x < 150 && y > 20 && y < 100) {
                gameState = GameState.Einstellungen;
            }

            System.out.println("x = " + x + ", y = " + y);
            if (gameState == GameState.Running) {
                int i = (int) x / 60;
                int j = (int) y / 60;
                if (x < 11 * 60) {
                    playerTargetPosition.set(i * 60 + 30, j * 60 + 30);
                }
            }
        } // if just touched

        if (gameState == GameState.Running) {
            if (playerPosition.x < playerTargetPosition.x) {
                playerPosition.x += 1;
            } else if (playerPosition.x > playerTargetPosition.x) {
                playerPosition.x -= 1;
            } else if (playerPosition.y < playerTargetPosition.y) {
                playerPosition.y += 1;
            } else if (playerPosition.y > playerTargetPosition.y) {
                playerPosition.y -= 1;
            }

            int i = (int) playerPosition.x / 60;
            int j = (int) playerPosition.y / 60;
            Texture t = raster[j][i];
            if (t == null) {
                raster[j][i] = log;
                loginv -= 1;
                if (soundOn) {
                    drop.play();
                }
            }
            if (loginv < 0) {
                gameState = GameState.GameOver;
                if (!isFailPlaying && soundOn) {
                    fail.play();
                    isFailPlaying = true;
                }
            }
            if (t == islandlog) {
                if (!isGivingLog) {
                    loginv += 5;
                    isGivingLog = true;
                }
            }
            if (t == shark1) {
                gameState = GameState.GameOver;
                if (!isFailPlaying && soundOn) {
                    fail.play();
                    isFailPlaying = true;
                }
            }
            if (t == finish && (playerPosition.x > i * 60 + 29 &&
                    playerPosition.y > j * 60 + 29)) {
                gameState = GameState.Win;
                if (!isClapPlaying && soundOn) {
                    clap.play();
                    isClapPlaying = true;
                }
            }

            // Kugeln animieren
            for (Cannonball k : cannonballs) {
                k.step();
                if (k.hits(playerPosition)) {
                    gameState = GameState.GameOver;
                }
            }

        } // if running
    }

    

    private void drawWorld() {
        camera.update();
        batch.setProjectionMatrix(camera.combined);
        batch.begin();

        int w2 = Gdx.graphics.getWidth() / 2;
        int h2 = Gdx.graphics.getHeight() / 2;
        if (gameState == GameState.Menu) {
            batch.draw(menu, 0, 0);
        }

        if (gameState == GameState.Running ||
                gameState == GameState.Ready ||
                gameState == GameState.GameOver ||
                gameState == GameState.Win ||
                gameState == GameState.Pause) {
            for (int y = 0; y < 8; y++) {
                for (int x = 0; x < 11; x++) {
                    Texture t = raster[y][x];
                    if (t != null) {
                        if (t == shark1) {
                            batch.draw(sharkanm.getKeyFrame(time), x * t.getWidth(), y * t.getHeight());
                        } else {
                            batch.draw(t, x * t.getWidth(), y * t.getHeight());
                        }
                    } else {
                        batch.draw(water, x * water.getWidth(), y * water.getHeight());
                    }
                }
            }
            //		batch.draw(finish, 10 * finish.getWidth(), 7 * finish.getHeight());
            //		batch.draw(start, 0 * start.getWidth(), 0 * start.getHeight());
            batch.draw(player.getKeyFrame(time), playerPosition.x - 30, playerPosition.y - 30);
            // System.out.println(time);

            //		if (gameState == GameState.GameOver || gameState == GameState.Running) {
            //font.draw(batch, "" + loginv, 100, 200);
            //		}

            for (Cannonball k : cannonballs) {
                batch.draw(kugel, k.position.x, k.position.y);
            }


        }

        if (gameState == GameState.GameOver) {
            batch.draw(gameOver, 0, 0);
        }
        if (gameState == GameState.Win) {
            batch.draw(gameSurvived, 0, 0);
        }

        if (gameState == GameState.Running ||
                gameState == GameState.GameOver ||
                gameState == GameState.Ready ||
                gameState == GameState.Pause ||
                gameState == GameState.Win) {
            batch.draw(frame, 11 * 60, 0);
            int i = 0;
            while (i < loginv) {
                batch.draw(loglonginv, 680, i * 20 + 174);
                i += 1;
            }
            if (loginv >= 0 && loginv < numbers.length) {
                batch.draw(numbers[loginv], 475, -100);
            }
        }
        if (gameState == GameState.InfoMenu) {
            batch.draw(infoMenu, 0, 0);
        }
        if (gameState == GameState.Info) {
            batch.draw(history, 0, 0);
        }
        if (gameState == GameState.Geschichte) {
            batch.draw(history, 0, 0);
        }
        if (gameState == GameState.Steuerung) {
            batch.draw(steering, 0, 0);
        }
        if (gameState == GameState.Running ||
                gameState == GameState.Ready ||
                gameState == GameState.GameOver ||
                gameState == GameState.Win ||
                gameState == GameState.Pause) {
            batch.draw(pauseButton, 700, 10);
        }
        if (gameState == GameState.Pause) {
            batch.draw(pause, 0, 0);
        }
        if (gameState == GameState.Einstellungen) {
            if (soundOn && hardmodeOff) {
                batch.draw(settingsSoundOnHardmodeOff, 0, 0);
            } else if (!soundOn && hardmodeOff) {
                batch.draw(settingsSoundOffHardmodeOff, 0, 0);
            } else if (soundOn && !hardmodeOff) {
                batch.draw(settingsSoundOnHardmodeOn, 0, 0);
            } else if (!soundOn && !hardmodeOff) {
                batch.draw(settingsSoundOffHardmodeOn, 0, 0);
            }

            resetWorld();

        }
        if (gameState == GameState.HardModeInfo) {
            batch.draw(hardmodeinfo, 0, 0);
        }
        //if (gameState == GameState.Einstellungen && x > 25 && x < 300 && y > 240 && y < 290)


        batch.end();

    }

}
